#!/bin/sh

set -e

envsubst < /etc/nginx/default.conf.tpl > etc/ngnix/confg.d/default.conf
ngnix -g 'deamon off;'